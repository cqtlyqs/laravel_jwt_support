<?php

namespace Ktnw\JwtSupport\Providers;

use Illuminate\Support\ServiceProvider;

class JwtSupportServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes($this->getPublishFiles());
    }

    private function getPublishFiles(): array
    {
        $data       = $this->publishData();
        $srcData    = array_column($data, "src");
        $targetData = array_column($data, "target");
        $publishes  = [];
        for ($i = 0; $i < count($srcData); $i++) {
            $publishes[$srcData[$i]] = $targetData[$i];
        }
        return $publishes;
    }

    private function publishData(): array
    {
        return [
            ['src' => __DIR__ . '/../../config/jwt.php', 'target' => config_path('jwt.php')],
            ['src' => __DIR__ . '/../Console/Commands/JwtSupportCommand.php', 'target' => app_path("Console/Commands/JwtSupportCommand.php")],
            ['src' => __DIR__ . '/../Http/Middleware/JwtRefresh.php', 'target' => app_path("Http/Middleware/JwtRefresh.php")],
        ];
    }


}